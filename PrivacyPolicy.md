Privacy Policy
==============

YI Livestream respects your privacy: all the content consumed by the application
will only be used locally in order to render the functionality of the
application, and will not be stored in any intermediate server.

The only information that YI Livestream will retrieve from the user's Google
account is the list of active broadcasts; this information is not going to be
stored, not in locally in the device or anywhere else.

YI Livestream will use the user's Google account to create live broadcasts,
using the title and privacy status chosen by the user.

YI Livestream will not try to access any files belonging to the user.