PROJECT_VERSION = 0.4

APPLICATION_NAME = "it.mardy.yi-livestream"
# Use this function just to get the title extracted for translation
defineReplace(tr) { return($$1) }
APPLICATION_TITLE = $$tr("YI Livestream")

INSTALL_DIR=$$(INSTALL_DIR)
CONFIG(qtc)|!isEmpty(INSTALL_DIR) {
    INSTALL_PREFIX = /
} else {
    INSTALL_PREFIX = $${TOP_BUILD_DIR}/click
}
INSTALL_LIB_DIR = $${INSTALL_PREFIX}/lib/$$system("dpkg-architecture -qDEB_HOST_MULTIARCH")
CLICK_ARCH = $$system("dpkg-architecture -qDEB_HOST_ARCH")
