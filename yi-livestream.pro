TEMPLATE = subdirs
SUBDIRS = src

click.target = click
click.depends = install
click.commands = "click build click"
QMAKE_EXTRA_TARGETS += click

include(common-config.pri)

CLICK_DIR = $${INSTALL_PREFIX}

icon.files = data/yi-livestream.png
icon.path = $${CLICK_DIR}
INSTALLS += icon

QMAKE_SUBSTITUTES += data/yi-livestream.desktop.in
desktop.files = data/yi-livestream.desktop
desktop.path = $${CLICK_DIR}
INSTALLS += desktop

package.files = \
    data/yi-livestream.accounts \
    data/yi-livestream.apparmor
package.path = $${CLICK_DIR}
INSTALLS += package

QMAKE_SUBSTITUTES += data/manifest.json.in
manifest.files = data/manifest.json
manifest.path = $${CLICK_DIR}
INSTALLS += manifest

# specify the source files that should be included into
# the translation file, from those files a translation
# template is created in po/template.pot, to create a
# translation copy the template to e.g. de.po and edit the sources
UBUNTU_TRANSLATION_SOURCES+= \
    $$files(src/*.qml,true) \
    $$files(src/*.js,true) \
    $$files(common-config.pri,true) \

# specifies all translations files and makes sure they are
# compiled and installed into the right place in the click package
UBUNTU_PO_FILES+=$$files(po/*.po)

UBUNTU_TRANSLATION_DOMAIN=$${APPLICATION_NAME}

include(translations.pri)
