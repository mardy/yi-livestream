import QtQuick 2.5
import QtQuick.Layouts 1.2
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem

Page {
    id: root

    property var configuration: null
    property var resolutions: null
    property var _resolutionsModel: [ "1080p", "720p", "480p" ]

    signal done()

    header: PageHeader {
        title: i18n.tr("Select video resolution")
        flickable: flick
        leadingActionBar.actions: [
            Action {
                iconName: "back"
                onTriggered: root.saveAndClose()
            }
        ]
    }

    Flickable {
        id: flick
        anchors.fill: parent
        contentHeight: contentItem.childrenRect.height

        ColumnLayout {
            anchors { left: parent.left; right: parent.right; margins: units.gu(1) }

            ListItem.ItemSelector {
                id: resControl
                Layout.fillWidth: true
                Layout.fillHeight: true
                expanded: true
                selectedIndex: _resolutionsModel.indexOf(configuration.res)
                model: _resolutionsModel
                delegate: OptionSelectorDelegate {
                    text: resolutions[modelData]
                }
                onDelegateClicked: {
                    selectedIndex = index
                    root.saveAndClose()
                }
            }
        }
    }

    function saveAndClose() {
        configuration.res = _resolutionsModel[resControl.selectedIndex]
        done()
        pageStack.pop()
    }
}
