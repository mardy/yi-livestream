import QtQuick 2.5
import QtQuick.Layouts 1.2
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem

Page {
    id: root

    property var livestream: null

    signal done()

    header: PageHeader {
        title: i18n.tr("Configure custom livestream")
        flickable: flick
        leadingActionBar.actions: [
            Action {
                iconName: "back"
                onTriggered: root.saveAndClose()
            }
        ]
    }

    Flickable {
        id: flick
        anchors.fill: parent
        contentHeight: contentItem.childrenRect.height

        ColumnLayout {
            anchors { left: parent.left; right: parent.right; margins: units.gu(1) }

            Label {
                Layout.fillWidth: true
                text: i18n.tr("Choose a name (for local use only)")
            }

            TextField {
                id: nameField
                Layout.fillWidth: true
                text: root.livestream.name
                Keys.onReturnPressed: urlField.forceActiveFocus()
            }

            Label {
                Layout.topMargin: units.gu(1)
                Layout.fillWidth: true
                text: i18n.tr("Enter url")
            }

            TextField {
                id: urlField
                Layout.fillWidth: true
                text: root.livestream.url
                inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText | Qt.ImhUrlCharactersOnly
                Keys.onReturnPressed: root.saveAndClose()
            }
        }
    }

    function saveAndClose() {
        livestream.accountName = "custom"
        livestream.name = nameField.text
        livestream.url = urlField.text
        done()
        pageStack.pop()
    }
}
