import QtQuick 2.5
import QtQuick.Layouts 1.2
import Ubuntu.Components 1.3

Page {
    id: root

    property var configuration: null
    property var livestream: null

    header: PageHeader {
        title: i18n.tr("Livestream code")
        flickable: flick
        leadingActionBar.actions: [
            Action {
                iconName: "back"
                onTriggered: pageStack.pop()
            }
        ]
    }

    Flickable {
        id: flick
        anchors.fill: parent
        contentHeight: contentItem.childrenRect.height

        ColumnLayout {
            anchors { left: parent.left; right: parent.right; margins: units.gu(1) }

            Label {
                Layout.fillWidth: true
                horizontalAlignment: Text.AlignHCenter
                text: i18n.tr("Scan this code with the action camera")
                wrapMode: Text.Wrap
            }

            Image {
                Layout.preferredWidth: Math.min(parent.width, flick.height)
                Layout.preferredHeight: width
                fillMode: Image.PreserveAspectFit
                source: "image://qrcode/" + root.generateConfig()
            }
        }
    }

    function generateConfig() {
        var config = root.configuration
        config.url = root.livestream.url
        return Qt.btoa(JSON.stringify(config))
    }
}
